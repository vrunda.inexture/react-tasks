var server_echo;
var json = {
  json: JSON.stringify({
    a: 1,
    b: 2,
  }),
  delay: 3,
};
fetch('/echo/', {
  method: 'post',
  headers: {
    Accept: 'application/json, text/plain, */*',
    'Content-Type': 'application/json',
  },
  //unnecessary use of json.stringify ===> '"{\\"a\\":1,\\"b\\":2}"'
  body:
    'json=' +
    encodeURIComponent(JSON.stringify(json.json)) +
    '&delay=' +
    json.delay,
})
  .then(function (response) {
    //returns promise so can't access the echo (asynchronous code)
    server_echo = response.json().echo;
    return response.json(); //can't send another response
  })
  .then(function (result) {
    alert(result); //promise is an object
  })
  .catch(function (error) {
    console.log('Request failed', error);
  });
server_echo.forEach((element) => console.log(element)); //server_echo undefined
