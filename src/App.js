import axios from 'axios';
import { useState } from 'react';

const App = () => {
  const [words, setWords] = useState([]);

  const handleClick = () => {
    let word = window.prompt('Enter a word');
    if (word) {
      axios
        .get(`https://api.datamuse.com/words?rel_rhy=${word}`)
        .then((res) => {
          setWords(res.data);
        });
    }
  };

  return (
    <div className='App'>
      <button onClick={handleClick}>Click here to Find Rhyming words</button>
      <br />
      {words.map((rhymingWord, index) => (
        <ul key={index}>
          <li>{rhymingWord.word}</li>
        </ul>
      ))}
    </div>
  );
};

export default App;
